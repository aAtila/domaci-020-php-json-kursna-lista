<?php

include('db.php');


$sql = "SELECT * FROM konverzije";

$result = mysqli_query($connection,$sql) or die(mysqli_error($connection));

$niz = array();

foreach($result AS $row){

    $timestamp = $row['timestamp'];
    $temp_datum = explode(" ",$timestamp);
    $konv_datum = $temp_datum[0];

    $niz['konverzije'][$konv_datum][] = $row;

}

$file = fopen('data.json', 'w');
fwrite($file, json_encode($niz, JSON_FORCE_OBJECT));

header("Location: data.json");
die();
