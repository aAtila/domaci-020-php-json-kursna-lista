<?php include('db.php'); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>Kursna lista :: Konvertor valuta</title>
</head>
<body>
  
<section class="hero is-info is-large is-full">

  <div class="hero-head">
    <nav class="navbar">
      <div class="container">
        <div class="navbar-brand">
          <a class="navbar-item"><img src="konvertor-type-white.png" alt="Logo"></a>
          <span class="navbar-burger burger" data-target="navbarMenuHeroB">
          <span></span>
          <span></span>
          <span></span>
          </span>
        </div>
          <div id="navbarMenuHeroB" class="navbar-menu">
            <div class="navbar-end">
              <a class="navbar-item is-active">Home</a>
              <a class="navbar-item" href="json.php">JSON file</a>
              <a class="navbar-item" id="DnevniKurs" href="#">Dnevni Kurs</a>
              <span class="navbar-item">
              <a class="button is-info is-inverted" href="https://gitlab.com/aAtila/domaci-020-php-json-kursna-lista/">
                <span class="icon"><i class="fab fa-gitlab"></i></span><span>Source Code</span>
              </a>
              </span>
            </div>
          </div>
        </div>
      </div>
    </nav>
  </div>

  <div class="hero-body">
    <div class="container">
      <p class="subtitle has-text-centered">
      Jednostavan, pregledan i precizan online konvertor valuta.
      </p>

      <form action="convert.php" method="POST" enctype="multipart/form-data">

      <div class="block">
      <div class="control has-text-centered">
      <label class="radio"><input type="radio" name="kurs" value="kupovni" checked>Kupovni</label>
      <label class="radio"><input type="radio" name="kurs" value="srednji">Srednji</label>
      <label class="radio"><input type="radio" name="kurs" value="prodajni">Prodajni</label>
      </div>
      </div>

      <div class="block">
      <div class="control field has-addons">
      <p class="control">
      <span class="select is-large">
      <select type="text" name="valuta"  value="" required="required">
      <option selected="true" disabled>Valuta</option>;
      
        <?php

        $sql = "SELECT * FROM valuta WHERE kod != '' ORDER BY kod ASC";
        $result = mysqli_query($connection,$sql) or die(mysql_error());

        if (mysqli_num_rows($result)>0) {
        while ($record = mysqli_fetch_array($result,MYSQLI_BOTH))
        echo '<option value="'.$record['id'].'">'.strtoupper($record['kod']).'</option>';
        }

        ?>

      </select>
      </span>
      </p>
      <p class="control">
      <input class="input is-large" type="text" name="iznos" placeholder="Unesi iznos">
      </p>
      <p class="control">
      <input type="submit" value="Convert" name="convert" class="button is-success is-large">
      </p>
      </div>
      </div>
      </div>
      </form>

    </div>
  </div>

</section>

<div class="modal">
  <div class="modal-background"></div>
  <div class="modal-card">
    <header class="modal-card-head">
      <p class="modal-card-title">Dnevni Kurs</p>
      <button class="delete close" aria-label="close"></button>
    </header>
    <section class="modal-card-body">

      <table class="table is-striped is-fullwidth">
        <thead>
          <tr>
            <th>#</th>
            <th>Drzava</th>
            <th>Kod Valute</th>
            <th>Kupovni</th>
            <th>Srednji</th>
            <th>Prodajni</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>#</th>
            <th>Drzava</th>
            <th>Kod Valute</th>
            <th>Kupovni</th>
            <th>Srednji</th>
            <th>Prodajni</th>
          </tr>
        </tfoot>
        <tbody>
          <tr>
            <th>1</th>
            <td>Australija</td>
            <td><strong>AUD</strong></td>
            <td>54.44</td>
            <td>54.66</td>
            <td>55.01</td>
          </tr>
          <tr>
            <th>2</th>
            <td>Australija</td>
            <td><strong>AUD</strong></td>
            <td>54.44</td>
            <td>54.66</td>
            <td>55.01</td>
          </tr>
          <tr>
            <th>3</th>
            <td>Australija</td>
            <td><strong>AUD</strong></td>
            <td>54.44</td>
            <td>54.66</td>
            <td>55.01</td>
          </tr>
          <tr>
            <th>4</th>
            <td>Australija</td>
            <td><strong>AUD</strong></td>
            <td>54.44</td>
            <td>54.66</td>
            <td>55.01</td>
          </tr>
          <tr>
            <th>5</th>
            <td>Australija</td>
            <td><strong>AUD</strong></td>
            <td>54.44</td>
            <td>54.66</td>
            <td>55.01</td>
          </tr>
        </tbody>
      </table>

    </section>
    <footer class="modal-card-foot">
      <button class="button is-dark close">Zatvori</button>
    </footer>
  </div>
</div>

<script src="https://unpkg.com/jquery@3.3.1/dist/jquery.js"></script>

<script>
  $("#DnevniKurs").click(function() {
    $(".modal").addClass("is-active");  
  });
  
  $(".close").click(function() {
     $(".modal").removeClass("is-active");
  });
</script>

</body>
</html>