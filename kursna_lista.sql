-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 16, 2018 at 01:33 AM
-- Server version: 5.7.22-0ubuntu18.04.1
-- PHP Version: 7.2.4-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rest-api`
--

-- --------------------------------------------------------

--
-- Table structure for table `konverzije`
--

DROP TABLE IF EXISTS `konverzije`;
CREATE TABLE `konverzije` (
  `id` int(11) NOT NULL,
  `iznos` float NOT NULL,
  `valuta_kod` varchar(3) NOT NULL,
  `iznos_kursa` float NOT NULL,
  `konvertovani_iznos` decimal(12,2) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konverzije`
--

INSERT INTO `konverzije` (`id`, `iznos`, `valuta_kod`, `iznos_kursa`, `konvertovani_iznos`, `timestamp`) VALUES
(1, 554, 'CAD', 77.7839, '43092.00', '2018-06-12 19:24:21'),
(2, 315, 'EUR', 117.838, '37118.97', '2018-06-12 19:28:06'),
(3, 1115, 'GBP', 134.903, '150416.85', '2018-06-12 19:32:31'),
(4, 1115, 'GBP', 134.903, '150416.85', '2018-06-15 19:32:31'),
(5, 315, 'EUR', 117.838, '37118.97', '2018-06-15 19:48:06'),
(6, 554, 'CAD', 77.7839, '43092.00', '2018-06-15 19:54:21'),
(7, 1234, 'CHF', 102.331, '126276.45', '2018-06-15 20:49:31'),
(8, 10, 'AUD', 76.4969, '764.97', '2018-06-15 20:49:42'),
(9, 1000, '', 15.9156, '15915.60', '2018-06-15 22:45:09'),
(10, 1000, '', 15.9156, '15915.60', '2018-06-15 22:49:49'),
(11, 1000, '', 15.9156, '15915.60', '2018-06-15 22:53:09'),
(12, 1000, '', 15.9156, '15915.60', '2018-06-15 22:53:14'),
(13, 1000, 'CAD', 77.5505, '77550.50', '2018-06-15 22:53:28'),
(14, 565, 'CHF', 102.024, '57643.56', '2018-06-15 23:19:55'),
(15, 454, 'EUR', 117.838, '53498.45', '2018-06-15 23:38:18'),
(16, 454, 'EUR', 117.838, '53498.45', '2018-06-15 23:46:24'),
(17, 454, 'EUR', 117.838, '53498.45', '2018-06-15 23:47:09'),
(18, 454, 'EUR', 117.838, '53498.45', '2018-06-15 23:47:40'),
(19, 454, 'EUR', 117.838, '53498.45', '2018-06-15 23:48:14'),
(20, 454, 'EUR', 117.838, '53498.45', '2018-06-15 23:48:31'),
(21, 1785, 'CHF', 102.331, '182660.84', '2018-06-15 23:48:52'),
(22, 1785, 'CHF', 102.638, '183208.83', '2018-06-15 23:50:54'),
(23, 1785, 'CHF', 102.638, '183208.83', '2018-06-15 23:51:52'),
(24, 1785, 'CHF', 102.638, '183208.83', '2018-06-15 23:53:51'),
(25, 1785, 'CHF', 102.638, '183208.83', '2018-06-15 23:54:19'),
(26, 1785, 'CHF', 102.638, '183208.83', '2018-06-15 23:54:30'),
(27, 1785, 'CHF', 102.638, '183208.83', '2018-06-15 23:55:24'),
(28, 1785, 'CHF', 102.638, '183208.83', '2018-06-15 23:55:42'),
(29, 1785, 'CHF', 102.638, '183208.83', '2018-06-15 23:56:28'),
(30, 1785, 'CHF', 102.638, '183208.83', '2018-06-15 23:57:19'),
(31, 1785, 'CHF', 102.638, '183208.83', '2018-06-15 23:57:32'),
(32, 1785, 'CHF', 102.638, '183208.83', '2018-06-15 23:58:22'),
(33, 1785, 'CHF', 102.638, '183208.83', '2018-06-15 23:58:32'),
(34, 1785, 'CHF', 102.638, '183208.83', '2018-06-15 23:59:05'),
(35, 1785, 'CHF', 102.638, '183208.83', '2018-06-15 23:59:19'),
(36, 1785, 'CHF', 102.638, '183208.83', '2018-06-15 23:59:31'),
(37, 780, 'GBP', 135.309, '105541.02', '2018-06-16 00:00:00'),
(38, 11, 'CAD', 77.5505, '853.06', '2018-06-16 00:37:04'),
(39, 333, 'USD', 102.408, '34101.86', '2018-06-16 01:25:26'),
(40, 333, 'USD', 102.408, '34101.86', '2018-06-16 01:25:57');

-- --------------------------------------------------------

--
-- Table structure for table `kurs_dinara`
--

DROP TABLE IF EXISTS `kurs_dinara`;
CREATE TABLE `kurs_dinara` (
  `id` int(11) NOT NULL,
  `datum` date NOT NULL,
  `valuta_id` int(11) NOT NULL,
  `kupovni` float NOT NULL,
  `srednji` float NOT NULL,
  `prodajni` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kurs_dinara`
--

INSERT INTO `kurs_dinara` (`id`, `datum`, `valuta_id`, `kupovni`, `srednji`, `prodajni`) VALUES
(1, '2018-06-15', 1, 117.838, 118.193, 118.547),
(2, '2018-06-15', 2, 101.795, 102.101, 102.408),
(3, '2018-06-15', 3, 102.024, 102.331, 102.638),
(4, '2018-06-15', 4, 134.903, 135.309, 135.715),
(5, '2018-06-15', 5, 76.0393, 76.2681, 76.4969),
(6, '2018-06-15', 6, 77.5505, 77.7839, 78.0173),
(7, '2018-06-15', 0, 11.6408, 11.6758, 11.7108),
(8, '2018-06-15', 0, 15.8144, 15.862, 15.9096),
(9, '2018-06-15', 0, 12.5105, 12.5481, 12.5857),
(10, '2018-06-15', 0, 0.918242, 0.921005, 0.923768),
(11, '2018-06-15', 0, 1.6287, 1.6336, 1.6385),
(12, '2018-06-15', 0, 15.8679, 15.9156, 15.9633);

-- --------------------------------------------------------

--
-- Table structure for table `valuta`
--

DROP TABLE IF EXISTS `valuta`;
CREATE TABLE `valuta` (
  `id` int(11) NOT NULL,
  `naziv` varchar(60) NOT NULL,
  `kod` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `valuta`
--

INSERT INTO `valuta` (`id`, `naziv`, `kod`) VALUES
(1, 'Evropska unija', 'eur'),
(2, 'SAD', 'usd'),
(3, 'Švajcarska', 'chf'),
(4, 'Velika Britanija', 'gbp'),
(5, 'Australija', 'aud'),
(6, 'Kanada', 'cad'),
(7, '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `konverzije`
--
ALTER TABLE `konverzije`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kurs_dinara`
--
ALTER TABLE `kurs_dinara`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `valuta`
--
ALTER TABLE `valuta`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `konverzije`
--
ALTER TABLE `konverzije`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `kurs_dinara`
--
ALTER TABLE `kurs_dinara`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `valuta`
--
ALTER TABLE `valuta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
