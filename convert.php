<?php

include('db.php');
include('convert.app.php');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
    <title>Kursna lista :: Konvertor valuta</title>
</head>
<body>
  
<section class="hero is-success is-large is-full">

  <div class="hero-head">
    <nav class="navbar">
      <div class="container">
        <div class="navbar-brand">
          <a class="navbar-item"><img src="konvertor-type-white.png" alt="Logo"></a>
          <span class="navbar-burger burger" data-target="navbarMenuHeroB">
          <span></span>
          <span></span>
          <span></span>
          </span>
        </div>
          <div id="navbarMenuHeroB" class="navbar-menu">
            <div class="navbar-end">
              <a class="navbar-item" href="/">Home</a>
              <a class="navbar-item" href="json.php">JSON file</a>
              <span class="navbar-item">
              <a class="button is-success is-inverted" href="https://gitlab.com/aAtila/domaci-020-php-json-kursna-lista/">
                <span class="icon"><i class="fab fa-gitlab"></i></span><span>Source Code</span>
              </a>
              </span>
            </div>
          </div>
        </div>
      </div>
    </nav>
  </div>

  <div class="hero-body">
    <div class="container">
      <p class="title has-text-centered">
        <span class="icon"><i class="far fa-3x fa-check-circle"></i></span><br><br>
        <?php echo $iznos . ' ' .$valuta_kod . ' = ' . $konvertovani_iznos; ?> RSD
      </p>
    </div>
  </div>

</section>

</body>
</html>